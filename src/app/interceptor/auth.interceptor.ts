import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpHeaders,
  HttpErrorResponse
} from '@angular/common/http';
import { Router } from '@angular/router';
import { catchError, Observable, throwError } from 'rxjs';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private router: Router) { }
  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {

    const userName: any = localStorage.getItem('email');
    const pwd: any = localStorage.getItem('pseudo');
    if (request.method !== 'GET' && request.url !== "http://localhost:8080/api/users/signup" || request.url === "http://localhost:8080/api/articles/user/me" || request.url === "http://localhost:8080/api/articles/user/published" || request.url === "http://localhost:8080/api/articles/user/draft") {
      console.log(request.url);

      const modifiedReq = request.clone({
        headers: new HttpHeaders({
          'Authorization': 'Basic ' + btoa(userName + ":" + pwd)

        })
      });
      return next.handle(modifiedReq).pipe(
        catchError((err: HttpErrorResponse) => {
          if (err.status === 401) {
            localStorage.removeItem("email");
            localStorage.removeItem("pseudo");
            this.router.navigate(["/login"]);
            console.log("hihihihihihihi")
          }
          return throwError(() => new Error());
        }));
    } 
    
    next.handle(request).subscribe((response:any)=>{
        console.log( response);
    })
    return next.handle(request);
  }

}
