import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreatePageComponent } from './pages/create-page/create-page.component';
import { HomeComponent } from './pages/home/home.component';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { QeustionPageComponent } from './pages/qeustion-page/qeustion-page.component';
import { SignupPageComponent } from './pages/signup-page/signup-page.component';
import { UserPageComponent } from './pages/user-page/user-page.component';

const routes: Routes = [{ path: '', component: HomeComponent },
{ path: 'qeustion/:id', component: QeustionPageComponent },
{ path: 'login', component: LoginPageComponent },
{ path: 'signup', component: SignupPageComponent },
{ path: 'create', component: CreatePageComponent },
{ path: 'user/:id', component: UserPageComponent }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
