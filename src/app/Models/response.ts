export interface Response{
    tid: string,
    content: string,
    date: string,
    pseudo: string,
    nbVote: number,
    userVote: string[],
    userTid: string
}