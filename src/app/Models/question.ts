export interface Question{
    tid: string,
    title: string,
    content: string,
    date: string,
    userPseudo: string,
    userTid: string,
    userEmail: string
}