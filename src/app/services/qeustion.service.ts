import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, Observable, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class QeustionService {
  constructor(private http: HttpClient) { }

  public getAllQeustions(indexPage: number): Observable<any> {
    return this.http.get("http://localhost:8080/api/qeustions?pageNo="+indexPage)
      .pipe(catchError((error: any) => this.handleError(error)));
  }
  public getUserQeustions(userTid: string): Observable<any> {
    return this.http.get("http://localhost:8080/api/qeustions/user/me?userTid="+userTid)
      .pipe(catchError((error: any) => this.handleError(error)));
  }
  public getUserResponse(userTid: string): Observable<any> {
    return this.http.get("http://localhost:8080/api/qeustions/response/user/"+userTid)
      .pipe(catchError((error: any) => this.handleError(error)));
  }
  public getQeustions(tid: string): Observable<any> {
    return this.http.get("http://localhost:8080/api/qeustions/"+tid)
      .pipe(catchError((error: any) => this.handleError(error)));
  }
  public getLanguages(): Observable<any> {
    return this.http.get("http://localhost:8080/api/qeustions/language")
      .pipe(catchError((error: any) => this.handleError(error)));
  }
  public createQuestion(body: any): Observable<any> {
    console.log(body);
    return this.http.post("http://localhost:8080/api/qeustions", body)
      .pipe(catchError((error: any) => this.handleError(error)));
  }
  public createResponse(body: any,tid: string): Observable<any> {
    return this.http.put("http://localhost:8080/api/qeustions/"+tid+"/response", body)
      .pipe(catchError((error: any) => this.handleError(error)));
  }
  public voteResponse(body: any): Observable<any> {
    return this.http.post("http://localhost:8080/api/qeustions/response/vote", body)
      .pipe(catchError((error: any) => this.handleError(error)));
  }
  public deleteVoteResponse(tid: string): Observable<any> {
    return this.http.delete("http://localhost:8080/api/qeustions/response/vote/"+tid)
      .pipe(catchError((error: any) => this.handleError(error)));
  }
  public login(): Observable<any> {
    const body:any=[]
    return this.http.post("http://localhost:8080/api/users/auth", body)
      .pipe(catchError((error: any) => this.handleError(error)));
  }
  public signUp(email: string,pseudo: string): Observable<any> {

    const body:any={
      email:email,
      pseudo:pseudo
    }
    return this.http.post("http://localhost:8080/api/users/signup", body)
      .pipe(catchError((error: any) => this.handleError(error)));
  }
  public filterByTilte(title: string): Observable<any> {
    return this.http.get("http://localhost:8080/api/qeustions/title?title="+title)
      .pipe(catchError((error: any) => this.handleError(error)));
  }
  public filterByLanguage(language: string): Observable<any> {
    return this.http.get("http://localhost:8080/api/qeustions/language/search?language="+language)
      .pipe(catchError((error: any) => this.handleError(error)));
  }
  public getQuestionWithoutResponse(): Observable<any> {
    return this.http.get("http://localhost:8080/api/qeustions/questionwithoutresponse")
      .pipe(catchError((error: any) => this.handleError(error)));
  }
  private handleError(error: any) {
    let message: string

    if (error.error instanceof ErrorEvent) {
      message = `${error.error.message}`
    } else {
      message = `${error.message}`
    }
    return throwError(() => new Error(message))
  }

}
