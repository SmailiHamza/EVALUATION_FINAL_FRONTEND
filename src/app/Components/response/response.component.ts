import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { Response } from 'src/app/Models/response';
import { QeustionService } from 'src/app/services/qeustion.service';

@Component({
  selector: 'app-response',
  templateUrl: './response.component.html',
  styleUrls: ['./response.component.scss']
})
export class ResponseComponent implements OnInit {
  @Input() response?: Response;
  @Output() newItemEvent = new EventEmitter<void>();
  public isVoted: boolean = false;
  public isConnected: boolean = false;
  public isMyResponse: boolean=false;
  constructor(private router: Router, private questionService: QeustionService) { }

  ngOnInit(): void {
    this.response!.date = moment(parseInt(this.response!.date)).fromNow();
    if (localStorage.getItem("email") == null || localStorage.getItem("pseudo") == null) {
      
    } else {
      this.isConnected = true;
      if(this.response?.userTid==localStorage.getItem("tid")!){
        this.isMyResponse=true;
      }else if (!this.response?.userVote.includes(localStorage.getItem("tid")!)) {
        this.isVoted = true;
      }
    }

  }
  vote(): void {
    const body = {
      responseTid: this.response?.tid
    }
    this.questionService.voteResponse(body).subscribe(res => {
      this.response = res;
      this.response!.date = moment(parseInt(this.response!.date)).fromNow();
      console.log(this.response)
   
      if (this.response?.userVote.includes(localStorage.getItem("tid")!)) {  
        this.isVoted = false;
      }
    });
  }
  deleteVote(){
    this.questionService.deleteVoteResponse(this.response?.tid!).subscribe(res => {
      this.response = res;
      this.response!.date = moment(parseInt(this.response!.date)).fromNow();
      console.log(this.response)
      if (!this.response?.userVote.includes(localStorage.getItem("tid")!)) {  
        this.isVoted = true;
      }
    });
  }

}
