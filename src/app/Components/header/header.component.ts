import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Input() isConnected: boolean=false;
  public tid: string | null =localStorage.getItem("tid");
  constructor(private router: Router) { }

  ngOnInit(): void {
  }
  logOut(){
    localStorage.removeItem("email");
    localStorage.removeItem("pseudo");
    localStorage.removeItem("tid");
    this.isConnected=false;
    this.router.navigate([""])
  }

}
