import { Component, Input, OnInit } from '@angular/core';
import * as moment from 'moment';
import { Question } from 'src/app/Models/question';

@Component({
  selector: 'app-qeustion',
  templateUrl: './qeustion.component.html',
  styleUrls: ['./qeustion.component.scss']
})
export class QeustionComponent implements OnInit {
  @Input() qeustion?: Question;
  @Input() isQeustionPage: boolean=false;
  constructor() { }

  ngOnInit(): void {
    this.qeustion!.date=moment(parseInt(this.qeustion!.date)).fromNow();
  }

}
