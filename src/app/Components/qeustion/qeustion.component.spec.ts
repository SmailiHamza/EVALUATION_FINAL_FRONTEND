import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QeustionComponent } from './qeustion.component';

describe('QeustionComponent', () => {
  let component: QeustionComponent;
  let fixture: ComponentFixture<QeustionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QeustionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QeustionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
