import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { QeustionService } from 'src/app/services/qeustion.service';

@Component({
  selector: 'app-new-response',
  templateUrl: './new-response.component.html',
  styleUrls: ['./new-response.component.scss']
})
export class NewResponseComponent implements OnInit {

    public loginForm!: FormGroup;
    public content: boolean = false;
    public isEmpty: boolean = false;
    public alert: boolean = false;
    public nbWord: number=0;
    @Input() tid?: string;
    @Output() newItemEvent=new EventEmitter<void>();
    constructor(private fb: FormBuilder, private qeustionService: QeustionService, private router: Router) { }
  
    ngOnInit(): void {
      this.loginForm = this.fb.group({
        content: ['', Validators.required]
      })
    }
    submitForm(ev: any) {
      ev.stopPropagation()
      if (this.loginForm.invalid) {
        this.isEmpty = true;
        setTimeout(() => this.isEmpty = false, 500);
        return
      }
  
      const body =this.loginForm.value;
        
      this.qeustionService.createResponse(body,this.tid!).subscribe({
        next: () => {
          this.alert = true;
          this.content = false;
          this.loginForm = this.fb.group({
            content: ['', Validators.required]
          })
          this.newItemEvent.emit();
          setTimeout(() => this.alert = false, 1500);
        },
        error: () => {
          this.alert = false;
        }
      })
    }
    input() {
      if (this.loginForm.value.content != null && this.loginForm.value.content != ""){
         const array:string []=this.loginForm.value.content.split(/\W+/);
      this.nbWord=array.length;
        this.content = true;
      }
      else {
        this.content = false;
      }
    }

}
