import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { QeustionService } from 'src/app/services/qeustion.service';
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  public loginForm!: FormGroup;
  public emailLabel: boolean = false;
  public pseudoLabel: boolean = false;
  public isEmpty: boolean = false;
  constructor(private fb: FormBuilder, private qeustionService: QeustionService, private router: Router) { }

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      email: ['', Validators.required],
      pseudo: ['', Validators.required]
    })
  }
  submitForm(ev: any) {
    ev.stopPropagation()

    if (this.loginForm.invalid) {
      this.isEmpty = true;
      setTimeout(() => this.isEmpty = false, 500);
      return
    }
    this.qeustionService.signUp(this.loginForm.value.email,this.loginForm.value.pseudo).subscribe({
      next: (response) => {
        this.router.navigate(["/"]);
        console.log(response);
      },
      error: (error) => {
        alert("Email ou pseudo exist déja!");
        console.log(error);
      }
    })
  }
  input() {
    if (this.loginForm.value.email != null && this.loginForm.value.email != "")
      this.emailLabel = true;
    else {
      this.emailLabel = false;
    }
    if (this.loginForm.value.pseudo != null && this.loginForm.value.pseudo != "")
      this.pseudoLabel = true;
    else {
      this.pseudoLabel = false;
    }
  }


}
