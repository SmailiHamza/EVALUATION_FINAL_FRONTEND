import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { QeustionComponent } from './Components/qeustion/qeustion.component';
import { HomeComponent } from './pages/home/home.component';
import { ResponseComponent } from './Components/response/response.component';
import { QeustionPageComponent } from './pages/qeustion-page/qeustion-page.component';
import { LucideAngularModule } from 'lucide-angular';
import { icons } from 'lucide-angular';
import { HeaderComponent } from './Components/header/header.component';
import { NgPipesModule } from 'ngx-pipes';
import { LoginComponent } from './Components/login/login.component';
import { SignupComponent } from './Components/signup/signup.component';
import { ReactiveFormsModule } from '@angular/forms';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { SignupPageComponent } from './pages/signup-page/signup-page.component';
import { AuthInterceptor } from './interceptor/auth.interceptor';
import { CreatePageComponent } from './pages/create-page/create-page.component';
import { NewResponseComponent } from './Components/new-response/new-response.component';
import { UserPageComponent } from './pages/user-page/user-page.component';

@NgModule({
  declarations: [
    AppComponent,
    QeustionComponent,
    HomeComponent,
    ResponseComponent,
    QeustionPageComponent,
    HeaderComponent,
    LoginComponent,
    SignupComponent,
    LoginPageComponent,
    SignupPageComponent,
    CreatePageComponent,
    NewResponseComponent,
    UserPageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    LucideAngularModule.pick(icons),
    NgPipesModule,
    ReactiveFormsModule
  ],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule { }
