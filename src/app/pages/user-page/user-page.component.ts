import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Question } from 'src/app/Models/question';
import { Response } from 'src/app/Models/response';
import { QeustionService } from 'src/app/services/qeustion.service';

@Component({
  selector: 'app-user-page',
  templateUrl: './user-page.component.html',
  styleUrls: ['./user-page.component.scss']
})
export class UserPageComponent implements OnInit {

  public qeustionList: Question[]=[];
  public responseList: Response[]=[];
  public isConnected: boolean=false;
  public isLoaded: boolean=false;
  public displayQuestion: boolean=true;
  constructor(private qeustionService: QeustionService,private activeRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.activeRoute.params.subscribe((params) => {
    this.qeustionService.getUserQeustions(params['id']).subscribe(response =>{
      console.log(response);
      this.qeustionList=response;
      this.isLoaded=true;
      this.displayQuestion=true;
    })
    if(localStorage.getItem("email")!=null && localStorage.getItem("pseudo")!=null){
      this.isConnected=true;
    } 
  });
  }
  getResponses(){
    this.activeRoute.params.subscribe((params) => {
      this.qeustionService.getUserResponse(params['id']).subscribe(response =>{
        console.log(response);
        this.responseList=response;
        this.isLoaded=true;
        this.displayQuestion=false;
      })
    });
    if(localStorage.getItem("email")!=null && localStorage.getItem("pseudo")!=null){
      this.isConnected=true;
    } 
  }
}
