import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QeustionPageComponent } from './qeustion-page.component';

describe('QeustionPageComponent', () => {
  let component: QeustionPageComponent;
  let fixture: ComponentFixture<QeustionPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QeustionPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QeustionPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
