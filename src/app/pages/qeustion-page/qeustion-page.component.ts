import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import { Question } from 'src/app/Models/question';
import { Response } from 'src/app/Models/response';
import { QeustionService } from 'src/app/services/qeustion.service';

@Component({
  selector: 'app-qeustion-page',
  templateUrl: './qeustion-page.component.html',
  styleUrls: ['./qeustion-page.component.scss']
})
export class QeustionPageComponent implements OnInit {
  public responseList: Response[]=[];
  public question?: Question;
  public isLoaded=false;
  public isConnected=false;
  public tid: string="";
  constructor(private qeustionService: QeustionService,private activeRoute: ActivatedRoute) { }

  ngOnInit(): void {
    if(localStorage.getItem("email")!=null && localStorage.getItem("pseudo")!=null){
      this.isConnected=true;
    } 
    this.activeRoute.params.subscribe((params) => {
      this.tid=params['id'];
    this.qeustionService.getQeustions(params['id']).subscribe(response =>{
      this.responseList=response.responseList;
      this.question=response;
      this.isLoaded=true;
    })});
  }
  refresh(): void {
    this.activeRoute.params.subscribe((params) => {
      this.tid=params['id'];
    this.qeustionService.getQeustions(params['id']).subscribe(response =>{
      this.responseList=response.responseList;
      this.question=response;
      this.question!.date=moment(parseInt(this.question!.date)).fromNow();
      this.isLoaded=true;
    })});
  }
}
