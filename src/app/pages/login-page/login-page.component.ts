import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {
  public isConnected: boolean=false;
  constructor(private router: Router) { }

  ngOnInit(): void {
    if(localStorage.getItem("email")!=null && localStorage.getItem("pseudo")!=null){
      this.router.navigate(["/"]);
    }else{
      this.isConnected=false;
    }
  }

}
