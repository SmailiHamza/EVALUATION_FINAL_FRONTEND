import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup-page',
  templateUrl: './signup-page.component.html',
  styleUrls: ['./signup-page.component.scss']
})
export class SignupPageComponent implements OnInit {
  public isConnected: boolean = false;
  constructor(private router: Router) { }

  ngOnInit(): void {
    if (localStorage.getItem("email") == null || localStorage.getItem("pseudo") == null) {
      this.isConnected = false;
      
    } else {
      this.isConnected = true;
     this.router.navigate(["/"]);
    }
  }

}
