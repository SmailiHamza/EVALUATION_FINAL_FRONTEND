import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Question } from 'src/app/Models/question';
import { QeustionService } from 'src/app/services/qeustion.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public loginForm!: FormGroup;
  public qeustionList: Question[]=[];
  public nbPage: number[]=[];
  public isConnected: boolean=false;
  public languages: string[]=[];
  public language: string="alllanguage";
  public withoutResponse: boolean=false;
  constructor(private qeustionService: QeustionService,private fb: FormBuilder,private questionService: QeustionService) { }

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      title: ['', Validators.required],
      language: ['', Validators.required]
    })
    this.qeustionService.getAllQeustions(0).subscribe(response =>{
      console.log(response);
      this.nbPage=new Array(response.nbPage);
      this.qeustionList=response.qeustionsList;
    })
    if(localStorage.getItem("email")!=null && localStorage.getItem("pseudo")!=null){
      this.isConnected=true;
    } 
    this.qeustionService.getLanguages().subscribe(response =>{
      this.languages=response;
    })
  }
  goToPage(event: any){
    this.qeustionService.getAllQeustions(event.target.value).subscribe(response =>{
      console.log(response);
      this.nbPage=new Array(response.nbPage);
      this.qeustionList=response.qeustionsList;
    })
  }
  filterByTilte() : void{
    this.withoutResponse=false;
    if(this.loginForm.value.title!=''){
      this.qeustionService.filterByTilte(this.loginForm.value.title).subscribe((response) => {
        console.log(response);
        this.qeustionList = response;
      });
    }else{
      this.ngOnInit();
    } 
  }
  filterByLanguage(event: any){
    this.withoutResponse=false;
    this.language=event.target.value;
    if(this.language!=="alllanguage"){
       this.qeustionService.filterByLanguage(this.language).subscribe(res=>{
      this.qeustionList=res;
    })
    }
  }
  questionWithoutResponse(){
    if(this.withoutResponse){
      this.withoutResponse=false;
    }else{  
    this.qeustionService.getQuestionWithoutResponse().subscribe(res=>{
      this.qeustionList=res;
      this.withoutResponse=true;
    })
  }
  }
  
}
