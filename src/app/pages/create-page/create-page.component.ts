import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { QeustionService } from 'src/app/services/qeustion.service';

@Component({
  selector: 'app-create-page',
  templateUrl: './create-page.component.html',
  styleUrls: ['./create-page.component.scss']
})
export class CreatePageComponent implements OnInit {
  
    public loginForm!: FormGroup;
    public title: boolean = false;
    public content: boolean = false;
    public isEmpty: boolean = false;
    public alert: boolean = false;
    public isConnected: boolean = false;
    public nbWord: number=0;
    public languages: string[]=[];
    public language: string="";
    constructor(private fb: FormBuilder, private qeustionService: QeustionService, private router: Router) { }
  
    ngOnInit(): void {
      this.loginForm = this.fb.group({
        title: ['', Validators.required],
        content: ['', Validators.required]
      })
      if (localStorage.getItem("email") == null || localStorage.getItem("pseudo") == null) {
        this.router.navigate(["/login"]);
      } else {
        this.isConnected = true;
      }
      this.qeustionService.getLanguages().subscribe(response =>{
        this.languages=response;
        this.language=this.languages[0];
      })
    }
    submitForm(ev: any) {
      ev.stopPropagation()
      const array:string []=this.loginForm.value.title.split(/\W+/);
      if (this.loginForm.invalid || array.length>20) {
        this.isEmpty = true;
        setTimeout(() => this.isEmpty = false, 500);
        return
      }
  
      const body = {
        ...this.loginForm.value,
        language:this.language
      }
        
      this.qeustionService.createQuestion(body).subscribe({
        next: () => {
          this.alert = true;
          this.title = false;
          this.content = false;
          this.loginForm = this.fb.group({
            title: ['', Validators.required],
            content: ['', Validators.required]
          })
          setTimeout(() => this.alert = false, 1500);
        },
        error: () => {
          this.alert = false;
        }
      })
    }
    input() {
      if (this.loginForm.value.title != null && this.loginForm.value.title != ""){
      const array:string []=this.loginForm.value.title.split(/\W+/);
      this.nbWord=array.length;
      this.title = true;
      }
      else {
        this.title = false;
      }
      if (this.loginForm.value.content != null && this.loginForm.value.content != "")
        this.content = true;
      else {
        this.content = false;
      }
    }
    getLanguage(event: any){
      this.language=event.target.value;
    }
  }
  
